import 'package:app_foxylabs/src/bloc/provider_bloc.dart';
import 'package:app_foxylabs/src/pages/login/login.dart';
import 'package:app_foxylabs/src/routes/routes.dart';
import 'package:app_foxylabs/src/utils/localstorage.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:app_foxylabs/src/theme/theme.dart';
import 'package:app_foxylabs/src/models/layout_model.dart';

import 'package:app_foxylabs/src/pages/launcher_tablet_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final localStorage = new LocalStorage();
  await localStorage.initPrefs();

  runApp(ProviderBloc(
    child: ChangeNotifierProvider(
      create: (_) => new LayoutModel(),
      child: ChangeNotifierProvider(
          create: (_) => new ThemeChanger(2), child: MyApp()),
    ),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;

    return MaterialApp(
      theme: currentTheme,
      debugShowCheckedModeBanner: false,
      title: 'Diseños App',
      routes: getApplicationRoutes(),
      home: OrientationBuilder(
        builder: (BuildContext context, Orientation orientation) {
          // print('Orientation $orientation');

          final screenSize = MediaQuery.of(context).size;

          if (screenSize.width > 500) {
            return LauncherTabletPage();
          } else {
            return LoginPage();
          }
        },
      ),
    );
  }
}
