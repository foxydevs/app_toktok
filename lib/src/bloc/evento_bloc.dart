import 'dart:io';
import 'package:app_foxylabs/src/model/evento_model.dart';
import 'package:app_foxylabs/src/provider/evento_provider.dart';
import 'package:rxdart/rxdart.dart';

class EventoBloc {
  //BEHAVIOR SUBJECT
  final _dataController = new BehaviorSubject<List<EventoModel>>();
  final _cargandoController  = new BehaviorSubject<bool>();
  //PROVIDER
  final _dataProvider   = new EventoProvider();

  Stream<List<EventoModel>> get dataStream => _dataController.stream;
  Stream<bool> get cargando => _cargandoController.stream;

  void getAll() async {
    _dataController.sink.add(null);
    final data = await _dataProvider.getAll();
    _dataController.sink.add( data );
  }

  void create( EventoModel dataModel ) async {
    _cargandoController.sink.add(true);
    await _dataProvider.create(dataModel);
    _cargandoController.sink.add(false);
  }

  Future<String> subirFoto( File foto ) async {
    _cargandoController.sink.add(true);
    final fotoUrl = await _dataProvider.subirImagen(foto);
    _cargandoController.sink.add(false);
    return fotoUrl;
  }

  void update( EventoModel dataModel ) async {
    _cargandoController.sink.add(true);
    await _dataProvider.update(dataModel);
    _cargandoController.sink.add(false);
  }

  void delete( String id ) async {
    await _dataProvider.delete(id);
  }

  dispose() {
    _dataController?.close();
    _cargandoController?.close();
  }

}