import 'package:app_foxylabs/src/bloc/categoria_bloc.dart';
import 'package:app_foxylabs/src/bloc/evento_bloc.dart';
import 'package:app_foxylabs/src/bloc/usuario_bloc.dart';
import 'package:flutter/material.dart';

class ProviderBloc extends InheritedWidget {
  final _usuarioBloc = new UsuarioBloc();
  final _eventoBloc = new EventoBloc();
  final _categoriaBloc = new CategoriaBloc();

  static ProviderBloc _instancia;

  factory ProviderBloc({Key key, Widget child}) {
    if (_instancia == null) {
      _instancia = new ProviderBloc._internal(key: key, child: child);
    }
    return _instancia;
  }

  ProviderBloc._internal({Key key, Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static UsuarioBloc usuarioBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProviderBloc>()
        ._usuarioBloc;
  }

  static EventoBloc eventoBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProviderBloc>()
        ._eventoBloc;
  }

  static CategoriaBloc categoriaBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProviderBloc>()
        ._categoriaBloc;
  }
}
