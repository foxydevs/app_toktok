import 'dart:convert';
import 'dart:io';
import 'package:app_foxylabs/src/model/categoria_model.dart';
import 'package:app_foxylabs/src/model/evento_model.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime_type/mime_type.dart';

import '../../config.dart';

class CategoriaProvider {
  final String _url = urlPath;
  //HEADERS
  Map<String, String> headers = {"Content-type": "application/json"};

  Future<bool> create(CategoriaModel data) async {
    final url = '$_url/categorias/add';
    final resp = await http.post(url,
        body: categoriaModelToJson(data), headers: headers);
    final decodedData = json.decode(resp.body);
    print(decodedData);
    return true;
  }

  Future<bool> update(CategoriaModel data) async {
    final url = '$_url/categorias/update_${data.id}';
    final resp =
        await http.put(url, body: categoriaModelToJson(data), headers: headers);
    final decodedData = json.decode(resp.body);
    print(decodedData);
    return true;
  }

  Future<List<CategoriaModel>> getAll() async {
    final url = '$_url/categorias/get_all';
    final resp = await http.get(url);
    final List<dynamic> decodedData = json.decode(resp.body);
    print(decodedData);

    final List<CategoriaModel> listData = new List();
    if (decodedData == null) return [];
    //if ( decodedData['error'] != null ) return [];

    decodedData.forEach((data) {
      final dataTemp = CategoriaModel.fromJson(data);
      //dataTemp.eveId = int.parse(data.id);
      print(dataTemp);
      listData.add(dataTemp);
    });

    return listData;
  }

  Future<int> delete(String id) async {
    final url = '$_url/categorias/delete_$id';
    final resp = await http.delete(url);
    print(resp.body);
    return 1;
  }

  Future<String> subirImagen(File imagen) async {
    final url = Uri.parse(urlCloudinary);
    final mimeType = mime(imagen.path).split('/'); //image/jpeg

    final imageUploadRequest = http.MultipartRequest('POST', url)
      ..fields['folder'] = 'categorias/usuario';

    final file = await http.MultipartFile.fromPath('file', imagen.path,
        contentType: MediaType(mimeType[0], mimeType[1]));

    imageUploadRequest.files.add(file);

    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if (resp.statusCode != 200 && resp.statusCode != 201) {
      print('Algo salio mal');
      print(resp.body);
      return null;
    }

    final respData = json.decode(resp.body);
    print(respData);

    return respData['secure_url'];
  }
}
