import 'dart:convert';
import 'package:app_foxylabs/src/model/secuencia_model.dart';
import 'package:http/http.dart' as http;
import '../../config.dart';

class SecuenciaProvider {
  final String _url = urlPath;
  //HEADERS 
  Map<String, String> headers = {"Content-type": "application/json"};

  Future<bool> create( SecuenciaModel data ) async {
    final url = '$_url/secuencias/add';
    final resp = await http.post( url, body: secuenciaModelToJson(data), headers: headers );
    final decodedData = json.decode(resp.body);
    print( decodedData );
    return true;
  }

  Future<bool> update( SecuenciaModel data ) async {
    final url = '$_url/secuencias/update_${ data.id }';
    final resp = await http.put( url, body: secuenciaModelToJson(data), headers: headers );
    final decodedData = json.decode(resp.body);
    print( decodedData );
    return true;
  }

  Future<List<SecuenciaModel>> getAll() async {
    final url  = '$_url/secuencias/get_all';
    final resp = await http.get(url);
    final List<dynamic> decodedData = json.decode(resp.body);
    print(decodedData);

    final List<SecuenciaModel> listData = new List();
    if ( decodedData == null ) return [];
    //if ( decodedData['error'] != null ) return [];

    decodedData.forEach( ( data ){
      final dataTemp = SecuenciaModel.fromJson(data);
      //dataTemp.eveId = int.parse(data.id);
      print(dataTemp);
      listData.add( dataTemp );
    });

    return listData;
  }

  Future<int> delete( String id ) async { 
    final url  = '$_url/secuencias/delete_$id';
    final resp = await http.delete(url);
    print( resp.body );
    return 1;
  }

}