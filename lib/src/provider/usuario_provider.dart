import 'dart:convert';
import 'dart:io';
import 'package:app_foxylabs/src/model/usuario_model.dart';
import 'package:app_foxylabs/src/utils/localstorage.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime_type/mime_type.dart';

import '../../config.dart';

class UsuarioProvider {
  final String _url = urlPath;
  //HEADERS 
  Map<String, String> headers = {"Content-type": "application/json"};

  Future<bool> create( UsuarioModel data ) async {
    final url = '$_url/usuarios/add';
    final resp = await http.post( url, body: usuarioModelToJson(data), headers: headers );
    final decodedData = json.decode(resp.body);
    print( decodedData );
    return true;
  }

  Future<bool> update( UsuarioModel data ) async {
    final url = '$_url/usuarios/update_${ data.id }';
    final resp = await http.put( url, body: usuarioModelToJson(data), headers: headers );
    final decodedData = json.decode(resp.body);
    print( decodedData );
    return true;
  }

  Future<UsuarioModel> login( UsuarioModel data ) async {
    final _localStorage = new LocalStorage();
    UsuarioModel usuarioModel = UsuarioModel();
    final url = '$_url/usuarios/login';
    final resp = await http.post( url, body: usuarioModelToJson(data), headers: headers );
    final decodedData = json.decode(resp.body);
    if ( decodedData.containsKey('id') ) {
      //_prefs.idUsuario = decodedData['taxis_usuarios_id'].toString();
      usuarioModel = UsuarioModel.fromJson(decodedData);
      //_prefs.idUsuario = usuarioModel.taxisUsuariosId.toString();
      //_prefs.autentication = "Autentication";
      //_prefs.tipoUsuario = usuarioModel.taxisUsuariosTaxisUsuariosTiposId.toString();
      print("=========================");
      print(decodedData);
      //print(_prefs.tipoUsuario);
      _localStorage.idUsuario = usuarioModel.id.toString();
      _localStorage.nombre = usuarioModel.nombres;
      _localStorage.apellido = usuarioModel.apellidos;
      _localStorage.correo = usuarioModel.email;
      _localStorage.imagen = usuarioModel.img;
      _localStorage.phone = usuarioModel.numero;


      /**
       * REGISTRAR NOTIFICACION PUSH
       */
      /*PushModel pushModel = new PushModel();
      pushModel.usuarioId = usuarioModel.id;
      pushModel.token = _localStorage.tokenPush;
      pushModel.tipoUsuario = usuarioModel.type;
      PushProvider pushProvider = new PushProvider();
      pushProvider.create(pushModel);*/

    } else {
      usuarioModel = null;
    }
    return usuarioModel;
  }

  Future<List<UsuarioModel>> getAll() async {
    final url  = '$_url/usuarios/get_all';
    final resp = await http.get(url);
    final List<dynamic> decodedData = json.decode(resp.body);
    print(decodedData);

    final List<UsuarioModel> listData = new List();
    if ( decodedData == null ) return [];
    //if ( decodedData['error'] != null ) return [];

    decodedData.forEach( ( data ){
      final dataTemp = UsuarioModel.fromJson(data);
      //dataTemp.eveId = int.parse(data.id);
      print(dataTemp);
      listData.add( dataTemp );
    });

    return listData;
  }

  Future<int> delete( String id ) async { 
    final url  = '$_url/usuarios/delete_$id';
    final resp = await http.delete(url);
    print( resp.body );
    return 1;
  }


  Future<String> subirImagen( File imagen ) async {
    final url = Uri.parse(urlCloudinary);
    final mimeType = mime(imagen.path).split('/'); //image/jpeg

    final imageUploadRequest = http.MultipartRequest(
      'POST',
      url
    )..fields['folder'] = 'eventos/usuario';
    

    final file = await http.MultipartFile.fromPath(
      'file', imagen.path,
      contentType: MediaType( mimeType[0], mimeType[1] )
    );

    imageUploadRequest.files.add(file);


    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if ( resp.statusCode != 200 && resp.statusCode != 201 ) {
      print('Algo salio mal');
      print( resp.body );
      return null;
    }

    final respData = json.decode(resp.body);
    print( respData);

    return respData['secure_url'];
  }

}