import 'dart:convert';
import 'package:app_foxylabs/src/model/evento_tipo_model.dart';
import 'package:http/http.dart' as http;

import '../../config.dart';

class EventoTipoProvider {
  final String _url = urlPath;
  //HEADERS 
  Map<String, String> headers = {"Content-type": "application/json"};

  Future<bool> create( EventoTipoModel data ) async {
    final url = '$_url/eventos_tipos/add';
    final resp = await http.post( url, body: eventoTipoModelToJson(data), headers: headers );
    final decodedData = json.decode(resp.body);
    print( decodedData );
    return true;
  }

  Future<bool> update( EventoTipoModel data ) async {
    final url = '$_url/eventos_tipos/update_${ data.id }';
    final resp = await http.put( url, body: eventoTipoModelToJson(data), headers: headers );
    final decodedData = json.decode(resp.body);
    print( decodedData );
    return true;
  }

  Future<List<EventoTipoModel>> getAll() async {
    final url  = '$_url/eventos_tipos/get_all';
    final resp = await http.get(url);
    final List<dynamic> decodedData = json.decode(resp.body);
    print(decodedData);

    final List<EventoTipoModel> listData = new List();
    if ( decodedData == null ) return [];
    //if ( decodedData['error'] != null ) return [];

    decodedData.forEach( ( data ){
      final dataTemp = EventoTipoModel.fromJson(data);
      //dataTemp.eveId = int.parse(data.id);
      print(dataTemp);
      listData.add( dataTemp );
    });

    return listData;
  }

  Future<int> delete( String id ) async { 
    final url  = '$_url/eventos_tipos/delete_$id';
    final resp = await http.delete(url);
    print( resp.body );
    return 1;
  }
}