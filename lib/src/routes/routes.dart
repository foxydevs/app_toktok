import 'package:app_foxylabs/src/pages/login/login.dart';
import 'package:app_foxylabs/src/pages/register/register.dart';
import 'package:app_foxylabs/src/pages/user/categoria/categoria_user.dart';
import 'package:app_foxylabs/src/pages/user/evento/evento_form_user.dart';
import 'package:app_foxylabs/src/pages/user/evento/evento_user.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:app_foxylabs/src/pages/graficas_circulares_page.dart';
import 'package:app_foxylabs/src/pages/headers_page.dart';
import 'package:app_foxylabs/src/pages/pinterest_page.dart';
import 'package:app_foxylabs/src/pages/slider_list_page.dart';
import 'package:app_foxylabs/src/retos/cuadrado_animado_page.dart';
import 'package:app_foxylabs/src/pages/emergency_page.dart';
import 'package:app_foxylabs/src/pages/slideshow_page.dart';

final pageRoutes = <_Route>[
  _Route(FontAwesomeIcons.home, 'Iniciar Sesión', LoginPage()),
  _Route(FontAwesomeIcons.calendar, 'Eventos', EventoUser()),
  _Route(FontAwesomeIcons.appStore, 'Categoria', CategoriaUser()),
  _Route(FontAwesomeIcons.slideshare, 'Slideshow', SlideshowPage()),
  _Route(FontAwesomeIcons.ambulance, 'Emergencia', EmergencyPage()),
  _Route(FontAwesomeIcons.heading, 'Encabezados', HeadersPage()),
  _Route(FontAwesomeIcons.peopleCarry, 'Cuadro Animado', CuadradoAnimadoPage()),
  _Route(
      FontAwesomeIcons.circleNotch, 'Barra Progreso', GraficasCircularesPage()),
  _Route(FontAwesomeIcons.pinterest, 'Pinterest', PinterestPage()),
  _Route(FontAwesomeIcons.mobile, 'Slivers', SliverListPage()),
];

class _Route {
  final IconData icon;
  final String titulo;
  final Widget page;

  _Route(this.icon, this.titulo, this.page);
}

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    /**
     * LOGIN & REGISTRO
     */
    'login': (BuildContext context) => LoginPage(),
    'register': (BuildContext context) => RegisterPage(),
    /**
     * USUARIO
     */
    'evento': (BuildContext context) => EventoUser(),
    'evento-form': (BuildContext context) => EventoFormUser(),
    'categoria': (BuildContext context) => CategoriaUser(),
  };
}
