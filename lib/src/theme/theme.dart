import 'package:flutter/material.dart';

class ThemeChanger with ChangeNotifier {
  bool _darkTheme = false;
  bool _customTheme = false;

  ThemeData _currentTheme;

  bool get darkTheme => this._darkTheme;
  bool get customTheme => this._customTheme;
  ThemeData get currentTheme => this._currentTheme;

  ThemeChanger(int theme) {
    switch (theme) {
      case 1: // light
        _darkTheme = false;
        _customTheme = false;
        _currentTheme = ThemeData.light().copyWith(
          accentColor: Color(0xffD32F2F),
          bottomAppBarColor: Color(0xffD32F2F),
        );
        break;

      case 2: // Dark
        _darkTheme = true;
        _customTheme = false;
        _currentTheme = ThemeData.dark().copyWith(
          accentColor: Color.fromRGBO(166, 3, 3, 1.0),
          brightness: Brightness.dark,
          primaryColorDark: Color(0xffD32F2F),
          primaryColorLight: Colors.white,
          disabledColor: Color(0xff545454),
          dividerColor: Color(0xff333333),
          primaryColor: Color(0xffD32F2F),
          buttonColor: Color(0xffC66900),
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            backgroundColor: Color(0xffD32F2F),
          ),
          textTheme: TextTheme(
              bodyText1: TextStyle(color: Colors.white),
              bodyText2: TextStyle(color: Colors.white),
              button: TextStyle(color: Colors.white)),
        );
        break;

      case 3: // Dark
        _darkTheme = false;
        _customTheme = true;
        break;

      default:
        _darkTheme = false;
        _darkTheme = false;
        _currentTheme = ThemeData.light();
    }
  }

  set darkTheme(bool value) {
    _customTheme = false;
    _darkTheme = value;

    if (value) {
      _currentTheme = ThemeData.dark().copyWith(
        accentColor: Color.fromRGBO(166, 3, 3, 1.0),
        brightness: Brightness.dark,
        primaryColorDark: Color(0xffD32F2F),
        primaryColorLight: Colors.white,
        disabledColor: Color(0xff545454),
        dividerColor: Color(0xff333333),
        primaryColor: Color(0xffD32F2F),
        buttonColor: Color(0xffC66900),
        floatingActionButtonTheme:
            FloatingActionButtonThemeData(backgroundColor: Color(0xffD32F2F)),
        textTheme: TextTheme(
            //body1: TextStyle(color: Colors.white),
            button: TextStyle(color: Colors.white)),
      );
    } else {
      _currentTheme = ThemeData.light().copyWith(
          accentColor: Color(0xffD32F2F), bottomAppBarColor: Color(0xffD32F2F));
    }

    notifyListeners();
  }

  set customTheme(bool value) {
    _customTheme = value;
    _darkTheme = false;

    if (value) {
      _currentTheme = ThemeData.dark().copyWith(
        accentColor: Color(0xff48A0EB),
        primaryColorLight: Colors.white,
        scaffoldBackgroundColor: Color(0xff16202B),
        textTheme: TextTheme(
            //body1: TextStyle( color: Colors.white ),
            button: TextStyle(color: Colors.white)),
        // textTheme.body1.color
      );
    } else {
      _currentTheme = ThemeData.light();
    }

    notifyListeners();
  }
}
