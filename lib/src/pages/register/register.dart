//import 'dart:io';

import 'package:app_foxylabs/src/bloc/provider_bloc.dart';
import 'package:app_foxylabs/src/bloc/usuario_bloc.dart';
import 'package:app_foxylabs/src/model/usuario_model.dart';
import 'package:app_foxylabs/src/widgets/alert_widget.dart';
import 'package:app_foxylabs/src/widgets/fondo_widget.dart';
//import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';

import 'dart:io';
import 'package:intl/intl.dart';

import 'package:image_picker/image_picker.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  //PROPIEDADES
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  UsuarioModel _dataModel = new UsuarioModel();
  UsuarioBloc dataBloc;
  bool _guardando = false;
  File _image;
  final picker = ImagePicker();
  bool _obscureText = true;
  bool _obscureText2 = true;
  final formatDate = DateFormat("yyyy-MM-dd");

  //COLOR FORMULARIO
  final _textStyle = TextStyle(color: Colors.white);
  final _outlineInputBorder = OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.white),
    borderRadius: BorderRadius.circular(15.0),
  );

  /*
   * TOGGLE
   */
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  /*
   * TOGGLE
   */
  void _toggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }

  @override
  Widget build(BuildContext context) {
    dataBloc = ProviderBloc.usuarioBloc(context);

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
          title: Text("Registro de Usuario"),
          centerTitle: true,
          backgroundColor: Color.fromRGBO(166, 3, 3, 1.0),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.image),
              onPressed: _seleccionarFoto,
            ),
          ]),
      body: Center(
        child: Stack(
          children: <Widget>[FondoWidget(), _form()],
        ),
      ),
    );
  }

  _form() {
    return Container(
      height: double.infinity,
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                _mostrarFoto(),
                SizedBox(height: 30.0),
                _nombre(),
                SizedBox(height: 15.0),
                _apellido(),
                SizedBox(height: 15.0),
                _numero(),
                SizedBox(height: 15.0),
                //_fechaNacimiento(),
                SizedBox(height: 15.0),
                _correo(),
                SizedBox(height: 15.0),
                _password(),
                SizedBox(height: 15.0),
                _passwordRepeat(),
                SizedBox(height: 15.0),
                _crearBoton(),
                SizedBox(height: 15.0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _mostrarFoto() {
    if (_dataModel.img != null) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: FadeInImage(
          image: NetworkImage(_dataModel.img),
          placeholder: AssetImage('assets/gif/loading.gif'),
          height: 250.0,
          fit: BoxFit.contain,
        ),
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: _image == null
            ? Image(
                image: AssetImage('assets/img/foxylabs.png'),
                height: 250.0,
                fit: BoxFit.cover,
              )
            : Image.file(
                _image,
                height: 250.0,
                fit: BoxFit.cover,
              ),
      );
    }
  }

  Widget _nombre() {
    return TextFormField(
      initialValue: _dataModel.nombres,
      cursorColor: Colors.white,
      style: TextStyle(color: Colors.white),
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          hintText: 'Nombre',
          labelText: 'Nombre',
          focusedBorder: _outlineInputBorder,
          enabledBorder: _outlineInputBorder,
          errorBorder: _outlineInputBorder,
          hintStyle: _textStyle,
          labelStyle: _textStyle,
          suffixIcon: Icon(
            Icons.title,
            color: Colors.white,
          )),
      onSaved: (value) => _dataModel.nombres = value,
      validator: (value) {
        if (value.length < 1) {
          return 'El correo es requerido.';
        } else {
          return null;
        }
      },
    );
  }

  Widget _apellido() {
    return TextFormField(
      initialValue: _dataModel.apellidos,
      cursorColor: Colors.white,
      style: TextStyle(color: Colors.white),
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          hintText: 'Apellido',
          labelText: 'Apellido',
          focusedBorder: _outlineInputBorder,
          enabledBorder: _outlineInputBorder,
          errorBorder: _outlineInputBorder,
          hintStyle: _textStyle,
          labelStyle: _textStyle,
          suffixIcon: Icon(
            Icons.title,
            color: Colors.white,
          )),
      onSaved: (value) => _dataModel.apellidos = value,
      validator: (value) {
        if (value.length < 1) {
          return 'El apellido es requerido.';
        } else {
          return null;
        }
      },
    );
  }

  Widget _numero() {
    return TextFormField(
      initialValue: _dataModel.numero,
      cursorColor: Colors.white,
      style: TextStyle(color: Colors.white),
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          hintText: 'No. Teléfono',
          labelText: 'No. Teléfono',
          focusedBorder: _outlineInputBorder,
          enabledBorder: _outlineInputBorder,
          errorBorder: _outlineInputBorder,
          hintStyle: _textStyle,
          labelStyle: _textStyle,
          suffixIcon: Icon(
            Icons.phone_android,
            color: Colors.white,
          )),
      onSaved: (value) => _dataModel.numero = value,
      validator: (value) {
        if (value.length < 1) {
          return 'El no. teléfono es requerido.';
        } else {
          return null;
        }
      },
    );
  }

  Widget _correo() {
    return TextFormField(
      initialValue: _dataModel.email,
      cursorColor: Colors.white,
      style: TextStyle(color: Colors.white),
      keyboardType: TextInputType.emailAddress,
      textCapitalization: TextCapitalization.none,
      decoration: InputDecoration(
          hintText: 'Correo Electrónico',
          labelText: 'Correo Electrónico',
          focusedBorder: _outlineInputBorder,
          enabledBorder: _outlineInputBorder,
          errorBorder: _outlineInputBorder,
          hintStyle: _textStyle,
          labelStyle: _textStyle,
          suffixIcon: Icon(
            Icons.alternate_email,
            color: Colors.white,
          )),
      onSaved: (value) => _dataModel.email = value,
      validator: (value) {
        if (value.length < 1) {
          return 'El correo es requerido.';
        } else {
          return null;
        }
      },
    );
  }

  /*Widget _fechaNacimiento() {

      final format = DateFormat("yyyy-MM-dd");
      return DateTimeField(
        format: format,
          decoration: InputDecoration(
          hintText: 'Fecha de Nacimiento',
          labelText: 'Fecha de Nacimiento',
          focusedBorder: _outlineInputBorder,
          enabledBorder: _outlineInputBorder,
          hintStyle: _textStyle,
          labelStyle: _textStyle,
          suffixIcon: Icon( Icons.calendar_today, color: Colors.white, )
        ),
        onShowPicker: (context, currentValue) async {
          final date = await showDatePicker(
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100));
          if (date != null) {
            final time = TimeOfDay.fromDateTime(DateTime.parse("2013-07-23 12:00:00"));
            print(DateTimeField.combine(date, time));
            _dataModel.fechaNac = DateTimeField.combine(date, time).toString().split(" ")[0];
            return DateTimeField.combine(date, time);
          } else {
            print(currentValue);
            return currentValue;
          }
        },
      );

  }*/

  Widget _password() {
    return TextFormField(
      obscureText: _obscureText,
      initialValue: _dataModel.pass,
      cursorColor: Colors.white,
      decoration: InputDecoration(
          hintText: 'Contraseña',
          labelText: 'Contraseña',
          focusedBorder: _outlineInputBorder,
          enabledBorder: _outlineInputBorder,
          errorBorder: _outlineInputBorder,
          hintStyle: _textStyle,
          labelStyle: _textStyle,
          suffixIcon: IconButton(
            onPressed: _toggle,
            icon: Icon(_obscureText ? Icons.lock_outline : Icons.lock_open),
            color: Colors.white,
          )),
      style: TextStyle(color: Colors.white),
      onSaved: (value) => _dataModel.pass = value,
      validator: (value) {
        if (value.length < 1) {
          return 'La contraseña es requerida.';
        } else {
          return null;
        }
      },
    );
  }

  Widget _passwordRepeat() {
    return TextFormField(
      obscureText: _obscureText2,
      initialValue: _dataModel.passRepeat,
      cursorColor: Colors.white,
      decoration: InputDecoration(
          hintText: 'Confirmar Contraseña',
          labelText: 'Confirmar Contraseña',
          focusedBorder: _outlineInputBorder,
          enabledBorder: _outlineInputBorder,
          errorBorder: _outlineInputBorder,
          hintStyle: _textStyle,
          labelStyle: _textStyle,
          suffixIcon: IconButton(
            onPressed: _toggle2,
            icon: Icon(_obscureText2 ? Icons.lock_outline : Icons.lock_open),
            color: Colors.white,
          )),
      style: TextStyle(color: Colors.white),
      onSaved: (value) => _dataModel.passRepeat = value,
      validator: (value) {
        if (value.length < 1) {
          return 'La contraseña es requerida.';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearBoton() {
    return ButtonTheme(
      minWidth: double.infinity,
      height: 50.0,
      child: RaisedButton.icon(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Color.fromRGBO(38, 1, 1, 1.0),
        textColor: Colors.white,
        label: Text('Registrarme'.toUpperCase()),
        icon: Icon(Icons.save),
        onPressed: (_guardando) ? null : _submit,
      ),
    );
  }

  Future _seleccionarFoto() async {
    _procesarImagen(ImageSource.gallery);
  }

  Future _procesarImagen(ImageSource origen) async {
    final pickedFile = await picker.getImage(source: origen);
    if (pickedFile != null) {
      _image = File(pickedFile.path);
    }

    if (_image != null) {
      _dataModel.img = null;
    }
    setState(() {});
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;

    _dataModel.estado = 1;
    _dataModel.fecha = DateTime.now().toIso8601String().split('T')[0];
    _dataModel.hora = DateFormat('kk:mm:ss').format(DateTime.now());

    formKey.currentState.save();

    setState(() {
      _guardando = true;
    });

    if (_image != null) {
      _dataModel.img = await dataBloc.subirFoto(_image);
    }

    if (_dataModel.pass == _dataModel.passRepeat) {
      dataBloc.create(_dataModel);
      Navigator.pop(context);
      mostrarAlert(context, "Usuario Registrado",
          "Se ha registrado tu usario con éxito.");
    } else {
      setState(() {
        _guardando = false;
      });
      SnackBar snackBar =
          new SnackBar(content: new Text("Las contraseñas no son iguales"));
      scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
}
