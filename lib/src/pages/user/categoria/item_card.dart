import 'package:app_foxylabs/src/model/categoria_model.dart';
import 'package:flutter/material.dart';

class ItemCard extends StatelessWidget {
  final CategoriaModel dataModel;
  final Function press;
  const ItemCard({
    Key key,
    this.dataModel,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const kTextColor = Color(0xFF535353);
    const kTextLightColor = Color(0xFFACACAC);
    const kDefaultPaddin = 20.0;
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Container(
                padding: EdgeInsets.all(10),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Image.network(
                    dataModel.img,
                    fit: BoxFit.cover,
                  ),
                )),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: kDefaultPaddin / 4),
            child: Text(
              // products is out demo list
              dataModel.titulo,
              style: TextStyle(color: kTextLightColor),
            ),
          ),
          Text(
            "\$${dataModel.descripcion}",
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
