import 'dart:io';
import 'package:app_foxylabs/src/model/secuencia_model.dart';
import 'package:app_foxylabs/src/provider/secuencia_provider.dart';
import 'package:app_foxylabs/src/theme/theme.dart';
import 'package:app_foxylabs/src/utils/localstorage.dart';
import 'package:app_foxylabs/src/widgets/alert_widget.dart';
import 'package:intl/intl.dart';

import 'package:app_foxylabs/src/bloc/evento_bloc.dart';
import 'package:app_foxylabs/src/bloc/provider_bloc.dart';
import 'package:app_foxylabs/src/model/evento_model.dart';
import 'package:app_foxylabs/src/model/evento_tipo_model.dart';
import 'package:app_foxylabs/src/provider/evento_tipo_provider.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class EventoFormUser extends StatefulWidget {
  EventoFormUser({Key key}) : super(key: key);

  @override
  _EventoFormUserState createState() => _EventoFormUserState();
}

class _EventoFormUserState extends State<EventoFormUser> {
  //PROPIEDADES
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final _localStorage = new LocalStorage();
  EventoModel _dataModel = new EventoModel();
  EventoBloc _dataBloc;
  bool _guardando = false;
  File _image;
  final picker = ImagePicker();
  //FORM
  EventoTipoModel _opcionEventoTipo;
  List<EventoTipoModel> _tipoEventos = List();

  SecuenciaModel _opcionSecuencia;
  List<SecuenciaModel> _secuencias = List();

  EventoTipoModel _buscarBeneficio(int id) {
    EventoTipoModel m;
    _tipoEventos.forEach((data) {
      if (data.id == id) {
        m = data;
      }
    });
    return m;
  }

  SecuenciaModel _buscarSecuencia(int id) {
    SecuenciaModel m;
    _secuencias.forEach((data) {
      if (data.id == id) {
        m = data;
      }
    });
    return m;
  }

  Future<EventoTipoModel> _cargarEventos() async {
    final _tipoProvider = new EventoTipoProvider();
    final data = await _tipoProvider.getAll();
    data.forEach((data) {
      _tipoEventos.add(data);
    });
    setState(() {});
    return null;
  }

  Future<SecuenciaModel> _cargarSecuencias() async {
    final _secuenciaProvider = new SecuenciaProvider();
    final data = await _secuenciaProvider.getAll();
    data.forEach((data) {
      _secuencias.add(data);
    });
    setState(() {});
    return null;
  }

  @override
  void initState() {
    super.initState();
    this._cargarEventos();
    this._cargarSecuencias();
  }

  @override
  Widget build(BuildContext context) {
    final currentColor = Provider.of<ThemeChanger>(context).currentTheme;

    _dataBloc = ProviderBloc.eventoBloc(context);

    final EventoModel data = ModalRoute.of(context).settings.arguments;

    if (data != null) {
      _dataModel = data;
      _opcionEventoTipo = _buscarBeneficio(data.eventosTiposId);
      _opcionSecuencia = _buscarSecuencia(data.secuenciasId);
      print("=================PICTURE=====================");
      print(_dataModel.img);
    }

    return Scaffold(
      appBar: AppBar(
          backgroundColor: currentColor.accentColor,
          title: Text("Formulario"),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.photo_size_select_actual),
              onPressed: _seleccionarFoto,
            ),
            IconButton(
              icon: Icon(Icons.camera_alt),
              onPressed: _tomarFoto,
            ),
          ]),
      body: Center(
        child: Stack(
          children: <Widget>[
            //FondoWidget(),
            _form()
          ],
        ),
      ),
    );
  }

  _form() {
    return Container(
      height: double.infinity,
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                _mostrarFoto(),
                SizedBox(height: 15.0),
                _titulo(),
                SizedBox(height: 15.0),
                _descripcion(),
                SizedBox(height: 15.0),
                _precio(),
                SizedBox(height: 15.0),
                _crearDropdownTipo(),
                SizedBox(height: 15.0),
                _crearDropdownSecuencia(),
                SizedBox(height: 15.0),
                _crearBoton()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _titulo() {
    return TextFormField(
      initialValue: _dataModel.titulo,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          hintText: 'Titulo',
          labelText: 'Titulo',
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          suffixIcon: Icon(Icons.title)),
      onSaved: (value) => _dataModel.titulo = value,
      validator: (value) {
        if (value.length < 1) {
          return 'El titulo es requerido.';
        } else {
          return null;
        }
      },
    );
  }

  Widget _descripcion() {
    return TextFormField(
      maxLines: null,
      keyboardType: TextInputType.multiline,
      initialValue: _dataModel.descripcion,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          hintText: 'Descripción',
          labelText: 'Descripción',
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          suffixIcon: Icon(Icons.description)),
      onSaved: (value) => _dataModel.descripcion = value,
      validator: (value) {
        if (value.length < 1) {
          return 'La descripción es requerida.';
        } else {
          return null;
        }
      },
    );
  }

  Widget _precio() {
    return TextFormField(
      initialValue: _dataModel.eventosPrecio.toStringAsFixed(2),
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          hintText: 'Precio',
          labelText: 'Precio',
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          suffixIcon: Icon(Icons.account_balance_wallet)),
      onSaved: (value) => _dataModel.eventosPrecio = double.parse(value),
      validator: (value) {
        if (value.length < 1) {
          return 'El precio es requerido.';
        } else {
          return null;
        }
      },
    );
  }

  List<DropdownMenuItem<EventoTipoModel>> getBeneficios() {
    List<DropdownMenuItem<EventoTipoModel>> lista = new List();
    _tipoEventos.forEach((data) {
      lista.add(DropdownMenuItem(
        child: Text(data.titulo),
        value: data,
      ));
    });
    return lista;
  }

  Widget _crearDropdownTipo() {
    return FormField<EventoTipoModel>(
      builder: (FormFieldState<EventoTipoModel> state) {
        return InputDecorator(
          decoration: InputDecoration(
            hintText: 'Tipo Evento ',
            labelText: 'Tipo Evento ',
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
          ),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              value: _opcionEventoTipo,
              items: getBeneficios(),
              onChanged: (opt) {
                _opcionEventoTipo = opt;
                _dataModel.eventosTiposId = _opcionEventoTipo.id;
                setState(() {});
              },
            ),
          ),
        );
      },
    );
  }

  List<DropdownMenuItem<SecuenciaModel>> getSecuencias() {
    List<DropdownMenuItem<SecuenciaModel>> lista = new List();
    _secuencias.forEach((data) {
      lista.add(DropdownMenuItem(
        child: Text(data.titulo),
        value: data,
      ));
    });
    return lista;
  }

  Widget _crearDropdownSecuencia() {
    return FormField<EventoTipoModel>(
      builder: (FormFieldState<EventoTipoModel> state) {
        return InputDecorator(
          decoration: InputDecoration(
            hintText: 'Secuencia ',
            labelText: 'Secuencia ',
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
          ),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              value: _opcionSecuencia,
              items: getSecuencias(),
              onChanged: (opt) {
                _opcionSecuencia = opt;
                _dataModel.secuenciasId = _opcionSecuencia.id;
                setState(() {});
              },
            ),
          ),
        );
      },
    );
  }

  Widget _mostrarFoto() {
    if (_dataModel.img != null) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: FadeInImage(
          image: NetworkImage(_dataModel.img),
          placeholder: AssetImage('assets/gif/loading.gif'),
          height: 250.0,
          fit: BoxFit.contain,
        ),
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: _image == null
            ? Image(
                image: AssetImage('assets/img/foxylabs.png'),
                height: 250.0,
                fit: BoxFit.cover,
              )
            : Image.file(
                _image,
                height: 250.0,
                fit: BoxFit.cover,
              ),
      );
    }
  }

  Widget _crearBoton() {
    return ButtonTheme(
      minWidth: double.infinity,
      height: 50.0,
      child: RaisedButton.icon(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Color.fromRGBO(38, 1, 1, 1.0),
        textColor: Colors.white,
        label: Text('Guardar Cambios'),
        icon: Icon(Icons.save),
        onPressed: (_guardando) ? null : _submit,
      ),
    );
  }

  Future _seleccionarFoto() async {
    _procesarImagen(ImageSource.gallery);
  }

  Future _tomarFoto() async {
    _procesarImagen(ImageSource.camera);
  }

  Future _procesarImagen(ImageSource origen) async {
    final pickedFile = await picker.getImage(source: origen);
    if (pickedFile != null) {
      _image = File(pickedFile.path);
    }

    if (_image != null) {
      _dataModel.img = null;
    }
    setState(() {});
  }

  void _submit() async {
    _dataModel.estado = 1;
    _dataModel.fechFin = DateTime.now().toIso8601String().split('T')[0];
    _dataModel.fechIni = DateTime.now().toIso8601String().split('T')[0];
    _dataModel.fecha = DateTime.now().toIso8601String().split('T')[0];
    _dataModel.hora = DateFormat('kk:mm:ss').format(DateTime.now());
    _dataModel.usuariosId = int.parse(_localStorage.idUsuario);

    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();

    setState(() {
      _guardando = true;
    });

    if (_image != null) {
      _dataModel.img = await _dataBloc.subirFoto(_image);
    }

    if (_dataModel.id == null) {
      _dataBloc.create(_dataModel);
    } else {
      _dataBloc.update(_dataModel);
    }

    Navigator.pop(context);
    mostrarAlert(context, "Evento Guardado",
        "Sus cambios se han guardado exitosamente.");
  }
}
