import 'package:app_foxylabs/src/bloc/evento_bloc.dart';
import 'package:app_foxylabs/src/bloc/provider_bloc.dart';
import 'package:app_foxylabs/src/model/evento_model.dart';
import 'package:app_foxylabs/src/theme/theme.dart';
import 'package:app_foxylabs/src/widgets/alert_widget.dart';
import 'package:app_foxylabs/src/widgets/menu_principal.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class EventoUser extends StatefulWidget {
  static const TextStyle planetTitle =
      const TextStyle(fontWeight: FontWeight.w600, fontSize: 24.0);

  static const TextStyle planetLocation =
      const TextStyle(fontWeight: FontWeight.w300, fontSize: 16.0);

  @override
  _EventoUserState createState() => _EventoUserState();
}

class _EventoUserState extends State<EventoUser> {
  EventoBloc _dataBloc;
  //final _localStorage = new LocalStorage();

  @override
  Widget build(BuildContext context) {
    final currentColor = Provider.of<ThemeChanger>(context).currentTheme;

    //_localStorage.ultimaPagina = 'evento-user';
    _dataBloc = ProviderBloc.eventoBloc(context);
    _dataBloc.getAll();

    Future<Null> _refresh() async {
      print('refresh...');
      _dataBloc.getAll();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Eventos"),
        centerTitle: true,
        backgroundColor: currentColor.accentColor,
      ),
      drawer: MenuPrincipal(),
      body: RefreshIndicator(
          onRefresh: _refresh, child: _crearListado(_dataBloc)),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: currentColor.accentColor,
        onPressed: () {
          Navigator.pushNamed(context, 'evento-form')
              .then((value) => setState(() {}));
        },
        label: Text('Agregar'),
        icon: Icon(Icons.add),
      ),
    );
  }

  Widget _crearListado(EventoBloc _dataBloc) {
    return StreamBuilder(
      stream: _dataBloc.dataStream,
      builder:
          (BuildContext context, AsyncSnapshot<List<EventoModel>> snapshot) {
        if (snapshot.hasData) {
          final data = snapshot.data;

          return ListView.builder(
            padding: EdgeInsets.all(10.0),
            itemCount: data.length,
            itemBuilder: (context, i) =>
                _crearItem(context, _dataBloc, data[i]),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _crearItem(
      BuildContext context, EventoBloc _dataBloc, EventoModel _dataModel) {
    final planetCard = Container(
        height: 124.0,
        margin: EdgeInsets.only(left: 46.0),
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            Color.fromRGBO(255, 255, 255, 0.005),
            Color.fromRGBO(166, 3, 3, 1.0),
          ]),
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black12,
              blurRadius: 10.0,
              offset: Offset(0.0, 10.0),
            ),
          ],
        ),
        child: Container(
          margin: const EdgeInsets.only(left: 72.0),
          constraints: new BoxConstraints.expand(),
          child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  _dataModel.titulo,
                  style: EventoUser.planetTitle,
                ),
                Text(
                  _dataModel.descripcion,
                  style: EventoUser.planetLocation,
                ),
              ]),
        ));

    final planetThumbnail = Container(
      margin: EdgeInsets.symmetric(vertical: 16.0),
      alignment: FractionalOffset.centerLeft,
      child: Container(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: Image.network(
            _dataModel.img,
            fit: BoxFit.cover,
          ),
        ),
        height: 92.0,
        width: 92.0,
      ),
    );

    return GestureDetector(
      onTap: () {
        final act = _actionSheet(context, _dataBloc, _dataModel);
        showCupertinoModalPopup(
            context: context, builder: (BuildContext context) => act);
      },
      child: Container(
          height: 120.0,
          margin: const EdgeInsets.symmetric(
            vertical: 16.0,
            horizontal: 24.0,
          ),
          child: Stack(
            children: <Widget>[
              planetCard,
              planetThumbnail,
            ],
          )),
    );
  }

  _actionSheet(
      BuildContext context, EventoBloc _dataBloc, EventoModel _dataModel) {
    return CupertinoActionSheet(
      title: Text("Opciones"),
      actions: <Widget>[
        CupertinoActionSheetAction(
          child: Text("Actualizar"),
          onPressed: () {
            Navigator.pop(context, "Actualizar");
            Navigator.pushNamed(context, 'evento-form', arguments: _dataModel)
                .then((value) => setState(() {}));
          },
        ),
        CupertinoActionSheetAction(
          child: Text("Eliminar"),
          onPressed: () {
            Navigator.pop(context, "Eliminar");
            confirmarEliminacion(context, _dataBloc, _dataModel);
            //setState(() {});
          },
        ),
        CupertinoActionSheetAction(
          child: Text("Detalle"),
          onPressed: () {
            Navigator.pop(context, "Detalle");
            Navigator.pushNamed(context, 'fitness-sub-admin',
                    arguments: _dataModel)
                .then((value) => setState(() {}));
          },
        )
      ],
      cancelButton: CupertinoActionSheetAction(
        child: Text("Cancelar"),
        isDefaultAction: true,
        onPressed: () {
          Navigator.pop(context, "Cancel");
        },
      ),
    );
  }

  confirmarEliminacion(
      BuildContext context, EventoBloc _dataBloc, EventoModel _dataModel) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            title: Text("Confirmar eliminación"),
            backgroundColor: Colors.black,
            titleTextStyle: TextStyle(color: Colors.white),
            contentTextStyle: TextStyle(color: Colors.white),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                    "¿Desea eliminar el contenido?, No puede revertir los cambios"),
                ClipRRect(
                  borderRadius: BorderRadius.circular(15.0),
                  child: new Image(
                    image: new AssetImage("assets/img/foxylabs.png"),
                    height: 125,
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.center,
                  ),
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                textColor: Colors.white,
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                  _dataBloc.delete(_dataModel.id.toString());
                  mostrarAlert(context, "Programa Eliminado",
                      "Sus cambios se han guardado exitosamente.");
                  setState(() {});
                },
              ),
              FlatButton(
                textColor: Colors.white,
                child: Text('Cancelar'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }
}
