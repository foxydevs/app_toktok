import 'package:app_foxylabs/src/model/usuario_model.dart';
import 'package:app_foxylabs/src/provider/usuario_provider.dart';
import 'package:app_foxylabs/src/widgets/fondo_widget.dart';
import 'package:app_foxylabs/src/widgets/menu_principal.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  //PROPIEDADES
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  UsuarioModel _dataModel = new UsuarioModel();
  UsuarioProvider _usuarioProvider = new UsuarioProvider();
  bool _guardando = false;
  bool _obscureText = true;
  /*
   * TOGGLE
   */
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  //COLOR FORMULARIO
  final _textStyle = TextStyle(color: Colors.white);
  final _outlineInputBorder = OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.white),
    borderRadius: BorderRadius.circular(15.0),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      drawer: MenuPrincipal(),
      appBar: AppBar(
        title: Text("Iniciar Sesión"),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(166, 3, 3, 1.0),
      ),
      body: Center(
        child: Stack(
          children: <Widget>[FondoWidget(), _form()],
        ),
      ),
    );
  }

  _form() {
    return Container(
      height: double.infinity,
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                _mostrarFoto(),
                SizedBox(height: 30.0),
                _correo(),
                SizedBox(height: 15.0),
                _password(),
                SizedBox(height: 15.0),
                _crearBoton(),
                SizedBox(height: 15.0),
                _crearBotonRegistro(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _mostrarFoto() {
    final _size = MediaQuery.of(context).size;
    return Center(
        child: Container(
      width: _size.width * 0.7,
      padding: EdgeInsets.only(top: 75, left: 15, right: 15),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: Image(
          image: AssetImage('assets/img/foxylabs.png'),
          fit: BoxFit.cover,
        ),
      ),
    ));
  }

  Widget _correo() {
    return TextFormField(
      initialValue: _dataModel.email,
      cursorColor: Colors.white,
      style: TextStyle(color: Colors.white),
      keyboardType: TextInputType.emailAddress,
      textCapitalization: TextCapitalization.none,
      decoration: InputDecoration(
          hintText: 'Correo Electrónico',
          labelText: 'Correo Electrónico',
          focusedBorder: _outlineInputBorder,
          enabledBorder: _outlineInputBorder,
          errorBorder: _outlineInputBorder,
          hintStyle: _textStyle,
          labelStyle: _textStyle,
          suffixIcon: Icon(
            Icons.alternate_email,
            color: Colors.white,
          )),
      onSaved: (value) => _dataModel.email = value,
      validator: (value) {
        if (value.length < 1) {
          return 'El correo es requerido.';
        } else {
          return null;
        }
      },
    );
  }

  Widget _password() {
    return TextFormField(
      initialValue: _dataModel.pass,
      cursorColor: Colors.white,
      obscureText: _obscureText,
      decoration: InputDecoration(
          hintText: 'Contraseña',
          labelText: 'Contraseña',
          focusedBorder: _outlineInputBorder,
          enabledBorder: _outlineInputBorder,
          errorBorder: _outlineInputBorder,
          hintStyle: _textStyle,
          labelStyle: _textStyle,
          suffixIcon: IconButton(
            onPressed: _toggle,
            icon: Icon(_obscureText ? Icons.lock_outline : Icons.lock_open),
            color: Colors.white,
          )),
      style: TextStyle(color: Colors.white),
      onSaved: (value) => _dataModel.pass = value,
      validator: (value) {
        if (value.length < 1) {
          return 'La contraseña es requerida.';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearBoton() {
    return ButtonTheme(
      minWidth: double.infinity,
      height: 50.0,
      child: RaisedButton.icon(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Color.fromRGBO(38, 1, 1, 1.0),
        textColor: Colors.white,
        label: Text('Iniciar Sesión'.toUpperCase()),
        icon: Icon(Icons.lock_open),
        onPressed: (_guardando) ? null : _submit,
      ),
    );
  }

  Widget _crearBotonRegistro() {
    return ButtonTheme(
      minWidth: double.infinity,
      height: 50.0,
      child: RaisedButton.icon(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Color.fromRGBO(38, 1, 1, 1.0),
        textColor: Colors.white,
        label: Text('Crear una nueva cuenta'.toUpperCase()),
        icon: Icon(Icons.person_add),
        onPressed: () => Navigator.pushNamed(context, 'register'),
      ),
    );
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();

    setState(() {
      _guardando = true;
    });

    UsuarioModel usuarioModel = await _usuarioProvider.login(_dataModel);
    if (usuarioModel != null) {
      //ADMINISTRADOR
      /*if(usuarioModel.id == 49) {
        Navigator.pushReplacementNamed(context, 'admin');
      } 
      //USUARIO
      else {
        Navigator.pushReplacementNamed(context, 'eventos');
      }*/
      Navigator.pushReplacementNamed(context, 'evento');
      setState(() {
        _guardando = false;
      });
      SnackBar snackBar = new SnackBar(content: new Text("LOGEADO"));
      scaffoldKey.currentState.showSnackBar(snackBar);
    } else {
      setState(() {
        _guardando = false;
      });
      SnackBar snackBar =
          new SnackBar(content: new Text("Usuario y contraseña inválidos."));
      scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
}
