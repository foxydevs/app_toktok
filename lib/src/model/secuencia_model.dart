import 'dart:convert';

SecuenciaModel secuenciaModelFromJson(String str) => SecuenciaModel.fromJson(json.decode(str));

String secuenciaModelToJson(SecuenciaModel data) => json.encode(data.toJson());

class SecuenciaModel {
    SecuenciaModel({
        this.id,
        this.titulo,
        this.descripcion,
        this.estado,
        this.fecha,
    });

    int id;
    String titulo;
    String descripcion;
    int estado;
    DateTime fecha;

    factory SecuenciaModel.fromJson(Map<String, dynamic> json) => SecuenciaModel(
        id: json["id"],
        titulo: json["titulo"],
        descripcion: json["descripcion"],
        estado: json["estado"],
        fecha: DateTime.parse(json["fecha"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "titulo": titulo,
        "descripcion": descripcion,
        "estado": estado,
        "fecha": fecha.toIso8601String(),
    };
}