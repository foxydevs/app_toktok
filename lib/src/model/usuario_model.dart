import 'dart:convert';

UsuarioModel usuarioModelFromJson(String str) => UsuarioModel.fromJson(json.decode(str));

String usuarioModelToJson(UsuarioModel data) => json.encode(data.toJson());

class UsuarioModel {
  UsuarioModel({
    this.id,
    this.nombres,
    this.apellidos,
    this.email,
    this.pass,
    this.passRepeat,
    this.img,
    this.numero,
    this.tocken,
    this.fechaNac,
    this.estado,
    this.fecha,
    this.hora,
  });

  int id;
  String nombres;
  String apellidos;
  String email;
  String pass;
  String passRepeat;
  String img;
  String numero;
  String tocken;
  String fechaNac;
  int estado;
  String fecha;
  String hora;

  factory UsuarioModel.fromJson(Map<String, dynamic> json) => UsuarioModel(
    id: json["id"],
    nombres: json["nombres"],
    apellidos: json["apellidos"],
    email: json["email"],
    pass: json["pass"],
    img: json["img"],
    numero: json["numero"],
    tocken: json["tocken"],
    fechaNac: json["fecha_nac"],
    estado: json["estado"],
    fecha: json["fecha"],
    hora: json["hora"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "nombres": nombres,
    "apellidos": apellidos,
    "email": email,
    "pass": pass,
    "img": img,
    "numero": numero,
    "tocken": tocken,
    "fecha_nac": fechaNac,
    "estado": estado,
    "fecha": fecha,
    "hora": hora,
  };
}
