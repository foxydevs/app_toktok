import 'dart:convert';

EventoModel eventoModelFromJson(String str) =>
    EventoModel.fromJson(json.decode(str));

String eventoModelToJson(EventoModel data) => json.encode(data.toJson());

class EventoModel {
  EventoModel({
    this.id,
    this.titulo,
    this.img,
    this.descripcion,
    this.fechIni,
    this.fechFin,
    this.eventosTiposId,
    this.secuenciasId,
    this.usuariosId,
    this.estado,
    this.fecha,
    this.hora,
    this.eventosPrecio = 0.00,
  });

  int id;
  String titulo;
  String img;
  String descripcion;
  String fechIni;
  String fechFin;
  int eventosTiposId;
  int secuenciasId;
  int usuariosId;
  int estado;
  String fecha;
  String hora;
  double eventosPrecio;

  factory EventoModel.fromJson(Map<String, dynamic> json) => EventoModel(
        id: json["id"],
        titulo: json["titulo"],
        img: json["img"],
        descripcion: json["descripcion"],
        fechIni: json["fech_ini"],
        fechFin: json["fech_fin"],
        eventosTiposId: json["eventos_tipos_id"],
        secuenciasId: json["secuencias_id"],
        usuariosId: json["usuarios_id"],
        estado: json["estado"],
        fecha: json["fecha"],
        hora: json["hora"],
        eventosPrecio: json["eventos_precio"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "titulo": titulo,
        "img": img,
        "descripcion": descripcion,
        "fech_ini": fechIni,
        "fech_fin": fechFin,
        "eventos_tipos_id": eventosTiposId,
        "secuencias_id": secuenciasId,
        "usuarios_id": usuariosId,
        "estado": estado,
        "fecha": fecha,
        "hora": hora,
        "eventos_precio": eventosPrecio,
      };
}
