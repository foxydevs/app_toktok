import 'dart:convert';

CategoriaModel categoriaModelFromJson(String str) => CategoriaModel.fromJson(json.decode(str));

String categoriaModelToJson(CategoriaModel data) => json.encode(data.toJson());

class CategoriaModel {
    CategoriaModel({
        this.id,
        this.titulo,
        this.descripcion,
        this.img,
        this.buscarId,
        this.secuenciasIngId,
        this.secuenciasEgrId,
        this.estado,
        this.fecha,
    });

    int id;
    String titulo;
    String descripcion;
    String img;
    int buscarId;
    int secuenciasIngId;
    int secuenciasEgrId;
    int estado;
    DateTime fecha;

    factory CategoriaModel.fromJson(Map<String, dynamic> json) => CategoriaModel(
        id: json["id"],
        titulo: json["titulo"],
        descripcion: json["descripcion"],
        img: json["img"],
        buscarId: json["buscar_id"],
        secuenciasIngId: json["secuencias_ing_id"],
        secuenciasEgrId: json["secuencias_egr_id"],
        estado: json["estado"],
        fecha: DateTime.parse(json["fecha"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "titulo": titulo,
        "descripcion": descripcion,
        "img": img,
        "buscar_id": buscarId,
        "secuencias_ing_id": secuenciasIngId,
        "secuencias_egr_id": secuenciasEgrId,
        "estado": estado,
        "fecha": fecha.toIso8601String(),
    };
}