import 'dart:convert';

EventoTipoModel eventoTipoModelFromJson(String str) => EventoTipoModel.fromJson(json.decode(str));

String eventoTipoModelToJson(EventoTipoModel data) => json.encode(data.toJson());

class EventoTipoModel {
    EventoTipoModel({
        this.id,
        this.titulo,
        this.descripcion,
        this.estado,
        this.fecha,
    });

    int id;
    String titulo;
    String descripcion;
    int estado;
    DateTime fecha;

    factory EventoTipoModel.fromJson(Map<String, dynamic> json) => EventoTipoModel(
        id: json["id"],
        titulo: json["titulo"],
        descripcion: json["descripcion"],
        estado: json["estado"],
        fecha: DateTime.parse(json["fecha"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "titulo": titulo,
        "descripcion": descripcion,
        "estado": estado,
        "fecha": fecha.toIso8601String(),
    };
}